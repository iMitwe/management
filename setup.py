from setuptools import setup, find_packages

import os

PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
packages=find_packages()

if 'REDISCLOUD_URL' in os.environ and 'REDISCLOUD_PORT' in os.environ and 'REDISCLOUD_PASSWORD' in os.environ:
     packages.append('django-redis-cache')
     packages.append('hiredis')

setup(name='MyCuteWebsite',
      version='1.0',
      description='OpenShift App using and Django-CMS Geodjango',
      author='iMitwe',
      author_email='example@example.com',
      url='https://pypi.python.org/pypi',
      packages=find_packages(),
      include_package_data=True,
      install_requires=open('%s/requirements.txt' % os.environ.get('OPENSHIFT_REPO_DIR', PROJECT_ROOT)).readlines(),
)
